<?php

/**
 * @file
 * Administrative callbacks for the Grand Drupal module.
 */


/**
 * Funtion used to create the settings form
 */
function _grand_drupal_settings_form($form, &$form_state) {
  $form['markup'] = array(
    '#type' => 'markup',
    '#markup' => t('Hello World, devsuitas....'),
  );

  return system_settings_form($form);
}

